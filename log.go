// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

package errors

import (
	"bytes"
	"fmt"
	"os"
)

type Context interface {
	Context() string
}

type Logger interface {
	Error(...interface{})
	Warn(...interface{})
	Access(...interface{})
	Info(...interface{})
	Debug(...interface{})
}

type Level int8

const (
	LevelError  Level = -1
	LevelWarn   Level = 0
	LevelAccess Level = 1
	LevelInfo   Level = 2
	LevelDebug  Level = 3
)

var out = make(chan []byte, 16)

func init() {
	go func() {
		for s := range out {
			os.Stderr.Write(s)
		}
	}()
}

func Log(parts ...interface{}) {
	buf := &bytes.Buffer{}
	for _, v := range parts {
		switch a := v.(type) {
		case error:
			buf.WriteString(a.Error())
		case Context:
			buf.WriteString(a.Context())
		case fmt.Stringer:
			buf.WriteString(a.String())
		case fmt.GoStringer:
			buf.WriteString(a.GoString())
		default:
			fmt.Fprint(buf, v)
		}
		buf.WriteByte(' ')
	}
	buf.WriteByte('\n')
	out <- buf.Bytes()
}
