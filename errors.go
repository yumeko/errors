// Copyright 2017 Yumeko Contributors (as named in meta/CONTRIBUTORS)
// This file is part of Yumeko (the "Larger Work"). Yumeko is only
// redistributable under the terms of the Common Development and
// Distribution License as written in the meta/LICENSE file

package errors

import (
	goerrors "errors"
)

type StackedError struct {
	cause error
	Stack []string
}

func (s StackedError) Error() string {
	c := s.cause.Error()
	tl := len(c) + 1 //\n
	for _, c := range s.Stack {
		tl += len(c) + 2 //\t\n
	}
	b := make([]byte, tl)
	i := copy(b, c)
	b[i] = '\n'
	i++
	for _, c := range s.Stack {
		b[i] = '\t'
		i++
		i += copy(b[i:], c)
		b[i] = '\n'
		i++
	}
	return string(b)
}

func (s StackedError) String() string {
	return s.Error()
}

func (s StackedError) Cause() error {
	return s.cause
}

func At(err error, reason string) error {
	if err == nil {
		return nil
	}
	var ok bool
	var s StackedError
	if s, ok = err.(StackedError); !ok {
		s.cause = err
	}
	s.Stack = append(s.Stack, reason)
	return s
}

func New(text string) error {
	return goerrors.New(text)
}

type Causer interface {
	Cause() error
}
